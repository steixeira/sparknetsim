package sparknetsim.stats

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD
import java.io._

object SimpsonsID {
  
  def main(args: Array[String]) {

    val popsize = args(1).toInt
    val mode = args(2) 
    val npartitions = args(3).toInt
    val outputdir = args(4)
				
		var conf = new SparkConf()
    
		if(mode == "local")
		  conf.setMaster("local").setAppName("SparkNetSimSID")
		else
		  conf.setAppName("SparkNetSimSID")
    
    val sc = new SparkContext(conf)
    
    val popData = sc.textFile(args(0), npartitions).cache()
    
    val popSIDTotal = (popsize * (popsize - 1)).toDouble
    
    var individuals = popData.map{ s => 
						val indiv = s.split("\t")
						(indiv(0), indiv(1))
		}.groupByKey().map{ geracao => 
		    val indiv = geracao._2.toVector
		    val ocorrencias = indiv.groupBy(identity).mapValues(_.size)
		    val SIDn = ocorrencias.map(i => (i._2 * (i._2 - 1))).reduce(_+_)
		    val SID : List[String] = List(geracao._1 + "\t" + (1 - (SIDn / popSIDTotal)))
		    (geracao._1, SID)  
		}

	  individuals.map(f => f._2.mkString(" ")).saveAsTextFile(outputdir + "outputSID")

		sc.stop()
		
  }
}