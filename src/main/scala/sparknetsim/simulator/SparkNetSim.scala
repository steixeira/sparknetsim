package sparknetsim.simulator

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD
import java.io._

object SparkNetSim {

	def main(args: Array[String]) {

		    val popsize = args(0).toInt
				val populations = args(1)
				val network = args(2)
				val mutrate = args(3).toFloat
				val recrate = args(4).toFloat
				val evols = args(5).toInt
				val exchs = args(6).toInt
				val generations = args(7).toInt
				val sampling = args(8).toInt
				val mode = args(9) 
				val npartitions = args(10).toInt
				val partition = args(11).toInt
				val outputdir = args(12)

				var conf = new SparkConf()

				if(mode == "local")
					conf.setMaster("local").setAppName("SparkNetSim")
					else{
						conf.setAppName("SparkNetSim")
					}

		    val sc = new SparkContext(conf)
		    val popData =  sc.textFile(populations,npartitions).cache()
				val  net = sc.textFile(network,npartitions).cache()
				
		    val startTime = System.currentTimeMillis()
		    
				val edges: RDD[Edge[Float]] = net.map { line => 
				val row = line.split("\t") 
				Edge(row(0).toInt, row(1).toInt, row(2).toFloat)
				} 

				var individuals = popData.map{ s => 
				val indiv = s.split("\t")
				(indiv(0).toLong, indiv(1).split(" ").map(_.trim))
				}.groupByKey().cache()

				val graph: Graph[Int, Float] = Graph.fromEdges(edges, defaultValue = 1)

				val r = scala.util.Random

				var geracao = 0

						for( sim <- 1 to generations){			
							
						  //Evolutions in each node
						  for ( i <- 1 to evols){
								geracao = ((sim -1) * evols + i)
								
								val newpop = individuals.map{x => 
  								val id = x._1
  								val values = x._2.toVector
  								
  								//Sampling for the next generation
  								var indiv : Vector[Array[String]] = scala.collection.immutable.Vector.empty
  								for(i <- 0 to popsize -1)
										indiv = indiv:+values(r.nextInt(popsize))   
										
									var afterevol : Vector[Array[String]] = scala.collection.immutable.Vector.empty
									var counter = 0
									for ( p <- 0 to popsize -1){
									    var ind = indiv(p).clone()
									    for ( al <- 0 to ind.size - 1){
									      //Recombination
											  if(r.nextFloat() < recrate) {
												  val randomone = indiv(r.nextInt(popsize)).clone()
													ind(al) = randomone(al)
												}
												//Mutation
												if(r.nextFloat() < mutrate){
											  	ind(al) = id + "." + geracao + "." + counter
													counter = counter +1
												}
											}
											afterevol = afterevol:+ind
									} 
									(id, afterevol.toIterable)
								}.cache() 				  
								
								individuals = newpop
								newpop.collect()
								if(geracao % sampling == 0)
							  	individuals.map(x => x._2.map(y => (geracao + "_"+ x._1 + "\t" + y.mkString(" ") + "\n"))).saveAsTextFile(outputdir + geracao)								
							}

						  //Exchanges between nodes
							for(ex <- 1 to exchs){ 		
								var newgraph = Graph(individuals, graph.edges)
										if(mode == "cluster" && partition == 1)
											newgraph = newgraph.partitionBy(PartitionStrategy.EdgePartition2D)
											val newpops = newgraph.triplets.map{ t =>
  											val key = t.srcId
  											val values = t.srcAttr.toVector
  											val dest = t.dstId
  											val proportion = (t.attr*values.size).toInt
  											//Samples to send
  											var s : Vector[Array[String]] = scala.collection.immutable.Vector.empty
  											if (t.attr != 1){
  												for(i<-0 to proportion-1)
  													s = s:+values(r.nextInt(popsize)).clone()
  											} else
  												s = values
  											(dest, s.toIterable)             
										  }.reduceByKey(_ ++ _).map{ pop => 
										    val key = pop._1
										    val values = pop._2.toVector
										    val proportion = values.size
										    //New Population after exchanges
										    var s : Vector[Array[String]] = scala.collection.immutable.Vector.empty
										    for(q <- 0 to popsize - 1)         
											    s = s:+values(r.nextInt(proportion)).clone()
											  (key, s.toIterable)   
										    }.cache()        
											individuals = newpops
											newpops.map(x => x._2.map(y => (geracao + "_T_" + ex + "_" + x._1 + "\t" + y.mkString(" ") + "\n"))).saveAsTextFile(outputdir + geracao + "_T_" + ex)
						  	}
						  }
				
				val endTime = System.currentTimeMillis();
				val duration = (endTime - startTime) / 1000;
				System.out.println("Simulator ended. Took " + duration + " seconds.");
				sc.stop()

	}
}